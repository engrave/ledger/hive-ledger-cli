import {SignedTransaction, Transaction} from '@hiveio/dhive'
import cli from 'cli-ux'
import HiveConnector from './hive-connector'
import Hive from '@engrave/ledger-app-hive'
import Transport from '@ledgerhq/hw-transport-node-hid-noevents'

export async function transactionHandler(logger: any, args: any, flags: any, transaction: Transaction) {
  cli.action.start('Establishing transport with Hive application')
  const transport = await Transport.create()
  cli.action.stop()
  const hive = new Hive(transport)

  let signed: SignedTransaction

  if (flags.blind) {
    const digest = hive.getTransactionDigest(transaction, flags.testnet ? HiveConnector.testnetChainId : HiveConnector.chainId);
    logger(`Hash: ${digest}`)
    cli.action.start('Review and confirm hash on your device')
    signed = {...transaction, signatures: [await hive.signHash(digest, args.path)]}
  } else {
    cli.action.start('Review and confirm transaction on your device')
    signed = await hive.signTransaction(transaction, args.path, flags.testnet ? HiveConnector.testnetChainId : HiveConnector.chainId)
  }

  cli.action.stop()
  if (flags.dry) {
    logger(JSON.stringify(signed, null, 3))
    return
  }
  const {id} = await HiveConnector.broadcast(signed, flags.testnet)
  logger(`Transaction ${id} broadcasted successfully\n`)
  if (flags.testnet) {
    cli.url('View it on test.ausbit.dev', `https://test.ausbit.dev/tx/${id}`)
  } else {
    cli.url('View it on hiveblockexplorer.com', `https://hiveblockexplorer.com/tx/${id}`)
  }

}
