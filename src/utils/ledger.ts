import cli from 'cli-ux'

export interface TransportError extends Error {
  statusCode: number;
  statusText: string;
}

export class Ledger {
  static translateError(error: TransportError) {
    switch (error.statusCode) {
    case 21260:
      cli.error('Ledger device is locked. Please unlock your device and open Hive application.')
      break
    case 0xB004:
      cli.error('SW_BAD_STATE: Security issue with bad application state. Restart Hive application on your device.')
      break
    case 0xB006:
      cli.error('Hash signing is disabled. Visit settings in Hive app on your device and enable hash signing.')
      break
    case 0x6D00:
      cli.error('Instruction not supported by the device. Make sure you have the latest app version.')
      break
    default:
      cli.error(error)
    }
  }
}
