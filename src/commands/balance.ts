import {Command, flags} from '@oclif/command'
import HiveConnector from '../utils/hive-connector'
import {Ledger} from '../utils/ledger'
import {
  Account,
  Asset,
  Client,
  DynamicGlobalProperties,
  ExtendedAccount,
  getVests,
} from '@hiveio/dhive'

export default class Balance extends Command {
  static description = 'display account balances'

  static examples = [
    `$ hive-ledger-cli balance
`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
  }

  static args = [
    {
      name: 'username',
      required: true,
      description: 'account name to check balance for',
    },
  ]

  async run() {
    const {args, flags} = this.parse(Balance)

    const client = HiveConnector.getClient(flags.testnet)

    try {
      const account = await this.getAccount(client, args.username)
      if (!account) {
        throw new Error('Invalid username provided')
      }

      const properties = await client.database.getDynamicGlobalProperties()

      this.log(`Balances for @${account.name} account`)
      this.log('----------------')
      this.log(`Liquid:  ${account.balance}, ${account.hbd_balance}`)
      this.log(`Savings: ${account.savings_balance}, ${account.savings_hbd_balance}`)
      this.log(`Staked:  ${this.getHivePower(account, properties).toString().replace('HIVE', 'HP')}`)
      this.log('----------------')
      this.log(`Pending rewards: ${(account.reward_vesting_hive as string).replace('HIVE', 'HP')}, ${account.reward_hive_balance}, ${account.reward_hbd_balance}`)
    } catch (error) {
      Ledger.translateError(error)
    }
  }

  async getAccount(client: Client, username: string): Promise<ExtendedAccount> {
    const [account] = await client.database.getAccounts([username])
    return account
  }

  getHivePower(account: Account, {total_vesting_fund_hive, total_vesting_shares}: DynamicGlobalProperties): Asset {
    const totalVestingShares = getVests(account)
    const hivePower = parseFloat(<string>total_vesting_fund_hive) * (totalVestingShares / parseFloat(<string>total_vesting_shares))
    return Asset.from(hivePower, 'HIVE')
  }
}
