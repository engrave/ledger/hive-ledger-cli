import {Command, flags} from '@oclif/command'
import HiveConnector from '../utils/hive-connector'
import {transactionHandler} from '../utils/transaction-handler'
import {Ledger} from '../utils/ledger'

export default class RecurrentTransfer extends Command {
  static description = 'create recurrent transfer from one account to another'

  static examples = [
    `$ hive-ledger-cli recurrent-transfer "m/48'/13'/0'/0'/0'" engrave nicniezgrublem "0.001 HIVE" 24 5 "Optional transfer description"
Establishing transport with Hive application... done
Review and confirm transaction on your device... done
Transaction broadcasted successfully with id eb88f7c23cf6e1d183e7bfbd12e204906b33af69

https://hiveblockexplorer.com/tx/eb88f7c23cf6e1d183e7bfbd12e204906b33af69

`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  static args = [
    {
      name: 'path',
      description: 'BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction',
      required: true,
    },
    {
      name: 'from',
      description: 'sender',
      required: true,
    },
    {
      name: 'to',
      description: 'receiver',
      required: true,
    },
    {
      name: 'amount',
      description: 'amount to send',
      required: true,
    },
    {
      name: 'recurrence',
      description: 'hours between consecutive transfers',
      required: true,
      parse: (input: string) => Number(input),
    },
    {
      name: 'executions',
      description: 'how many times should this transfer occur',
      required: true,
      parse: (input: string) => Number(input),
    },
    {
      name: 'memo',
      description: 'optional transfer memo',
    },
  ]

  async run() {
    const {args, flags} = this.parse(RecurrentTransfer)
    try {
      const transaction = await HiveConnector.prepareTransaction([['recurrent_transfer', {
        from: args.from,
        to: args.to,
        amount: args.amount,
        recurrence: args.recurrence,
        executions: args.executions,
        memo: args.memo ? args.memo : '',
        extensions: [],
      }]], flags.testnet)
      await transactionHandler(this.log, args, flags, transaction)
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
