import {Command, flags} from '@oclif/command'
import HiveConnector from '../utils/hive-connector'
import {transactionHandler} from '../utils/transaction-handler'
import {Ledger} from '../utils/ledger'

export default class TransferFromSavings extends Command {
  static description = 'transfer HIVE or HBD from savings account'

  static examples = [
    `$ hive-ledger-cli transfer-from-savings "m/48'/13'/0'/0'/0'" engrave 1 engrave "0.001 HIVE" "Test transfer"
Establishing transport with Hive application... done
Review and confirm transaction on your device... done
Transaction broadcasted successfully with id eb88f7c23cf6e1d183e7bfbd12e204906b33af69

https://hiveblockexplorer.com/tx/eb88f7c23cf6e1d183e7bfbd12e204906b33af69

`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  // .names = {"Operation", "From", "Request ID", "To", "Amount", "Memo"},

  static args = [
    {
      name: 'path',
      description: 'BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction',
      required: true,
    },
    {
      name: 'from',
      description: 'source account',
      required: true,
    },
    {
      name: 'requestId',
      description: 'request ID',
      required: true,
      parse: (input: string) => Number(input),
    },
    {
      name: 'to',
      description: 'target account',
      required: true,
    },
    {
      name: 'amount',
      description: 'amount to transfer',
      required: true,
    },
    {
      name: 'memo',
      description: 'optional transfer memo',
      required: false,
    },
  ]

  async run() {
    const {args, flags} = this.parse(TransferFromSavings)

    try {
      const transaction = await HiveConnector.prepareTransaction([['transfer_from_savings', {
        from: args.from,
        request_id: args.requestId,
        to: args.to,
        amount: args.amount,
        memo: args.memo,
      }]], flags.testnet)
      await transactionHandler(this.log, args, flags, transaction)
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
