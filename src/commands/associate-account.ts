import {Command, flags} from '@oclif/command'
import Transport from '@ledgerhq/hw-transport-node-hid-noevents'
import Hive from '@engrave/ledger-app-hive'
import cli from 'cli-ux'
import HiveConnector from '../utils/hive-connector'
import {Client, ExtendedAccount, PrivateKey} from '@hiveio/dhive'
import {AuthorityType} from '@hiveio/dhive/lib/chain/account'
import {Ledger} from '../utils/ledger'

enum Roles {
  owner = 0,
  active = 1,
  memo = 3,
  posting = 4
}

export default class AssociateAccount extends Command {
  static description = 'associate account with connected Ledger device. It will replace existing authorities with keys from your device'

  static examples = [
    `$ hive-ledger-cli associate-account test.ledger
Found existing account:  test.ledger

This operation will replace ALL your keys with those from your device. First, we need to find new keys for your owner, active and posting authorities.

Press any key to continue or q to exit:
Establishing transport with Hive application... done
Searching for unused keys on your device... done

New owner key:  STM7ZEBoDotbYpnyNHdARYMDBMNnLWpV7fiiGa6pvHbXhfRo9ZrDf
Derivation path:  m/48'/13'/0'/1'/0'
Please confirm this public key on your device... done

New active key:  STM5zpFRqa73yFULSwUYfPSftx4fE7kha9YfkAPR9yKvNLKU2QDFu
Derivation path:  m/48'/13'/1'/1'/0'
Please confirm this public key on your device... done

New posting key:  STM6YGUesBwuotbZvcfqoXfjQUrfVXGYwJZ9DCHEiFLQfxUsLK9M1
Derivation path:  m/48'/13'/4'/1'/0'
Please confirm this public key on your device... done

Now you need to enter your CURRENT OWNER PRIVATE KEY in WIF format. It will be used to update your account with a new keys. Please be aware that this operation will replace ALL of your current keys

Enter your current PRIVATE OWNER KEY for test.ledger:`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    'key-gap': flags.integer({
      char: 'k',
      description: 'Gap limit for unused key indexes on which the software decides that SLIP-0048 account index is not used',
      default: 5,
    }),
  }

  static args = [
    {
      name: 'username',
      required: true,
      description: 'Account to associate',
    },
  ]

  async run() {
    const {args, flags} = this.parse(AssociateAccount)

    const client = HiveConnector.getClient(flags.testnet)
    const prefix = HiveConnector.getAddressPrefix(flags.testnet)

    try {
      const account = await this.getAccount(client, args.username)

      if (!account) {
        throw new Error('Invalid username provided')
      }

      this.log('Found existing account: ', args.username)

      this.log('\nThis operation will replace ALL your keys with those from your device. First, we need to find new keys for your owner, active and posting authorities.\n')
      await cli.anykey()

      cli.action.start('Establishing transport with Hive application')
      const transport = await Transport.create()
      const hive = new Hive(transport)
      cli.action.stop()

      const accountIndex: number = await this.findUnusedAccountIndex(hive, client, flags['key-gap'], prefix)

      const owner = await this.confirmKey(hive, accountIndex, Roles.owner, prefix)
      const active = await this.confirmKey(hive, accountIndex, Roles.active, prefix)
      const posting = await this.confirmKey(hive, accountIndex, Roles.posting, prefix)

      this.log('\nNow you need to enter your CURRENT OWNER PRIVATE KEY in WIF format. It will be used to update your account with a new keys. Please be aware that this operation will replace ALL of your current keys\n')

      const input = await cli.prompt(`Enter your current PRIVATE OWNER KEY for ${args.username}`)

      await this.updateAccountWithKeys(client, account, PrivateKey.from(input), owner, active, posting, flags.testnet, flags.dry)

      this.log('Account was successfully updated with keys derived from your device. Please use `discover-account` command to refresh your account details')
    } catch (error) {
      Ledger.translateError(error)
    }
  }

  async getAccount(client: Client, username: string): Promise<ExtendedAccount> {
    const [account] = await client.database.getAccounts([username])
    return account
  }

  async findUnusedAccountIndex(transport: Hive, client: Client, gap: number, prefix: string): Promise<number> {
    cli.action.start('Searching for unused keys on your device')
    let accountIndex = 0
    // eslint-disable-next-line no-constant-condition
    while (true) {
      const keys: string[] = []
      for (let keyIndex = 0; keyIndex < gap; keyIndex++) {
        const path = `m/48'/13'/${Roles.owner}'/${accountIndex}'/${keyIndex}'`
        // eslint-disable-next-line no-await-in-loop
        const key = await transport.getPublicKey(path, false, prefix)
        keys.push(key)
      }
      // eslint-disable-next-line no-await-in-loop
      const {accounts} = await client.keys.getKeyReferences(keys)
      if (accounts.flat().length === 0) {
        cli.action.stop()
        return accountIndex
      }
      accountIndex++
    }
  }

  async confirmKey(transport: Hive, accountIndex: number, role: Roles, prefix: string): Promise<string> {
    const path = `m/48'/13'/${role}'/${accountIndex}'/${0}'`
    const key = await transport.getPublicKey(path, false, prefix)

    this.log(`\nNew ${Roles[role]} key: `, key)
    this.log('Derivation path: ', path)

    cli.action.start('Please confirm this public key on your device')
    await transport.getPublicKey(path, true, prefix)
    cli.action.stop()
    return key
  }

  async updateAccountWithKeys(client: Client, account: ExtendedAccount, currentOwnerKey: PrivateKey, owner: string, active: string, posting: string, testnet: boolean, dry: boolean) {
    const transaction = await HiveConnector.prepareTransaction([['account_update', {
      account: account.name,
      owner: this.createAuthority(owner),
      active: this.createAuthority(active),
      posting: this.createAuthority(posting),
      memo_key: account.memo_key,
      json_metadata: account.json_metadata,
    }]], testnet)

    const signedTx = await client.broadcast.sign(transaction, currentOwnerKey)

    if (dry) {
      cli.log(JSON.stringify(signedTx, null, 3))
    } else {
      const {id} = await client.broadcast.send(signedTx)
      cli.log(`Transaction ${id} broadcasted successfully\n`)
      if (testnet) {
        cli.url('View it on test.ausbit.dev', `https://test.ausbit.dev/tx/${id}`)
      } else {
        cli.url('View it on hiveblockexplorer.com', `https://hiveblockexplorer.com/tx/${id}`)
      }
    }
  }

  private createAuthority(key: string): AuthorityType {
    return {
      account_auths: [],
      key_auths: [[key, 1]],
      weight_threshold: 1,
    }
  }
}
