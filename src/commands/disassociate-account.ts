import {Command, flags} from '@oclif/command'
import cli from 'cli-ux'
import HiveConnector from '../utils/hive-connector'
import {Client, ExtendedAccount, PrivateKey} from '@hiveio/dhive'
import {AuthorityType} from '@hiveio/dhive/lib/chain/account'
import {Ledger} from '../utils/ledger'
import {transactionHandler} from '../utils/transaction-handler'

export default class DisassociateAccount extends Command {
  static description = 'disassociate account with connected Ledger device. It will replace existing authorities with keys derived from master password hyou provide, allowing you to use this account with other Hive frontends'

  static examples = [
    '$ hive-ledger-cli disassociate-account "m/48\'/13\'/0\'/0\'/0\'" test.ledger master-password',
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  static args = [
    {
      name: 'path',
      description: 'BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction',
      required: true,
    },
    {
      name: 'username',
      required: true,
      description: 'Account to associate',
    },
    {
      name: 'password',
      description: 'Hive compatible "master password" used to derive set of private keys to replace current keys',
      required: true,
    },
  ]

  async run() {
    const {args, flags} = this.parse(DisassociateAccount)

    const client = HiveConnector.getClient(flags.testnet)

    try {
      const account = await this.getAccount(client, args.username)

      if (!account) {
        throw new Error('Invalid username provided')
      }

      this.log('Found existing account: ', args.username)

      this.log('\nThis operation will replace ALL your keys. You will be no longer able to use ledger device to sign transactions, so please SAVE your generated private keys displayed in the next step\n')
      await cli.anykey()

      const owner = PrivateKey.fromLogin(args.name, args.password, 'owner')
      const active = PrivateKey.fromLogin(args.name, args.password, 'active')
      const posting = PrivateKey.fromLogin(args.name, args.password, 'posting')

      const data = [
        {role: 'owner', private_key: owner.toString(), public_key: owner.createPublic().toString()},
        {role: 'active', private_key: active.toString(), public_key: active.createPublic().toString()},
        {role: 'posting', private_key: posting.toString(), public_key: posting.createPublic().toString()},
      ]

      cli.table(data as any[], {
        role: {
          minWidth: 7,
        }, private_key: {
          minWidth: 7,
        }, public_key: {
          minWidth: 7,
        },
      })

      this.log('\nPlease save private keys displayed above.\n')

      await cli.anykey()

      const transaction = await HiveConnector.prepareTransaction([['account_update', {
        account: account.name,
        owner: this.createAuthority(owner.createPublic().toString()),
        active: this.createAuthority(active.createPublic().toString()),
        posting: this.createAuthority(posting.createPublic().toString()),
        memo_key: account.memo_key,
        json_metadata: account.json_metadata,
      }]], flags.testnet)
      await transactionHandler(this.log, args, flags, transaction)
    } catch (error) {
      console.log(error)
      Ledger.translateError(error)
    }
  }

  async getAccount(client: Client, username: string): Promise<ExtendedAccount> {
    const [account] = await client.database.getAccounts([username])
    return account
  }

  private createAuthority(key: string): AuthorityType {
    return {
      account_auths: [],
      key_auths: [[key, 1]],
      weight_threshold: 1,
    }
  }
}
