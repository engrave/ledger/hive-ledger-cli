import {Command, flags} from '@oclif/command'
import HiveConnector from '../utils/hive-connector'
import {transactionHandler} from '../utils/transaction-handler'
import {Ledger} from '../utils/ledger'

export default class LimitOrderCreate extends Command {
  static description = 'create limit order on internal market to sell or buy HIVE or HBD'

  static examples = [
    `$ hive-ledger-cli limit-order-create "m/48'/13'/0'/0'/0'" engrave 1 "0.001 HIVE" "0.001 HBD" false "2035-10-29T06:32:22"
Establishing transport with Hive application... done
Review and confirm transaction on your device... done
Transaction 1f0c8376cf989483dbb9c3d5fa1006ded00cb6c5 broadcasted successfully

https://test.ausbit.dev/tx/1f0c8376cf989483dbb9c3d5fa1006ded00cb6c5`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  static args = [
    {
      name: 'path',
      description: 'BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction',
      required: true,
    },
    {
      name: 'owner',
      description: 'account to perform limit order creation',
      required: true,
    },
    {
      name: 'orderid',
      description: 'order ID',
      required: true,
      parse: (input: string) => Number(input),
    },
    {
      name: 'amount_to_sell',
      description: 'amount to sell',
      required: true,
    },
    {
      name: 'min_to_receive',
      description: 'minimal amount to receive',
      required: true,
    },
    {
      name: 'fill_or_kill',
      description: 'set to true if you want your limit order to be immediately cancelled if cannot be fulfilled',
      required: true,
    },
    {
      name: 'expiration',
      description: 'order expiration date',
      required: true,
    },
  ]

  async run() {
    const {args, flags} = this.parse(LimitOrderCreate)

    try {
      const transaction = await HiveConnector.prepareTransaction([['limit_order_create', {
        owner: args.owner,
        orderid: args.orderid,
        amount_to_sell: args.amount_to_sell,
        min_to_receive: args.min_to_receive,
        fill_or_kill: args.fill_or_kill === 'true',
        expiration: args.expiration,
      }]], flags.testnet)
      await transactionHandler(this.log, args, flags, transaction)
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
