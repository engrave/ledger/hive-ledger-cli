import {Command, flags} from '@oclif/command'
import HiveConnector from '../utils/hive-connector'
import {transactionHandler} from '../utils/transaction-handler'
import {Ledger} from '../utils/ledger'

export default class TransferToVesting extends Command {
  static description = 'convert HIVE to Hive Power, aka stake'

  static examples = [
    `$ hive-ledger-cli transfer-to-vesting "m/48'/13'/0'/0'/0'" engrave nicniezgrublem "0.001 HIVE"
Establishing transport with Hive application... done
Review and confirm transaction on your device... done
Transaction broadcasted successfully with id eb88f7c23cf6e1d183e7bfbd12e204906b33af69

https://hiveblockexplorer.com/tx/eb88f7c23cf6e1d183e7bfbd12e204906b33af69

`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  static args = [
    {
      name: 'path',
      description: 'BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction',
      required: true,
    },
    {
      name: 'from',
      description: 'source account',
      required: true,
    },
    {
      name: 'to',
      description: 'target account',
      required: true,
    },
    {
      name: 'amount',
      description: 'amount to stake',
      required: true,
    },
  ]

  async run() {
    const {args, flags} = this.parse(TransferToVesting)

    try {
      const transaction = await HiveConnector.prepareTransaction([['transfer_to_vesting', {
        from: args.from,
        to: args.to,
        amount: args.amount,
      }]], flags.testnet)
      await transactionHandler(this.log, args, flags, transaction)
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
