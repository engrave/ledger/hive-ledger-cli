import {Command, flags} from '@oclif/command'
import HiveConnector from '../utils/hive-connector'
import {transactionHandler} from '../utils/transaction-handler'
import {Ledger} from '../utils/ledger'

export default class Vote extends Command {
  static description = 'vote on post or comment'

  static examples = [
    `$ hive-ledger-cli vote "m/48'/13'/0'/0'/0'" "engrave" "engrave" "introduction"
Establishing transport with Hive application... done
Review and confirm transaction on your device... done
Transaction broadcasted successfully with id a4f9d4694f9e3d45b939d75d3063239f1963f31a

https://hiveblockexplorer.com/tx/a4f9d4694f9e3d45b939d75d3063239f1963f31a`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  static args = [
    {
      name: 'path',
      description: 'BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction',
      required: true,
    },
    {
      name: 'voter',
      description: 'voter',
      required: true,
    },
    {
      name: 'author',
      description: 'post author',
      required: true,
    },
    {
      name: 'permlink',
      description: 'post permlink',
      required: true,
    },
    {
      name: 'weight',
      description: 'vote weight (10000 is 100%)',
      required: true,
      parse: (input: string) => Number(input),
    },
  ]

  async run() {
    const {args, flags} = this.parse(Vote)

    try {
      const transaction = await HiveConnector.prepareTransaction([['vote', {
        voter: args.voter,
        author: args.author,
        permlink: args.permlink,
        weight: args.weight,
      }]], flags.testnet)
      await transactionHandler(this.log, args, flags, transaction)
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
