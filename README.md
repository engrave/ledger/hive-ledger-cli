hive-ledger-cli
===============

CLI for Hive ledger wallet application

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/hive-ledger-cli.svg)](https://npmjs.org/package/hive-ledger-cli)
[![Downloads/week](https://img.shields.io/npm/dw/hive-ledger-cli.svg)](https://npmjs.org/package/hive-ledger-cli)
[![License](https://img.shields.io/npm/l/hive-ledger-cli.svg)](https://gitlab.com/engrave/ledger/hive-ledger-cli/-/blob/main/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g hive-ledger-cli
$ hive-ledger-cli COMMAND
running command...
$ hive-ledger-cli (-v|--version|version)
hive-ledger-cli/0.0.0 linux-x64 node-v14.18.0
$ hive-ledger-cli --help [COMMAND]
USAGE
  $ hive-ledger-cli COMMAND
...
```
<!-- usagestop -->

Because this CLI is using `node-hid` dependency, you may need to set `--unsafe-perm` flag when installing it globally. 

`sudo npm install -g hive-ledger-cli --unsafe-perm`

# Commands
<!-- commands -->
* [`hive-ledger-cli associate-account USERNAME`](#hive-ledger-cli-associate-account-username)
* [`hive-ledger-cli discover-accounts [ROLE]`](#hive-ledger-cli-discover-accounts-role)
* [`hive-ledger-cli get-public-key PATH`](#hive-ledger-cli-get-public-key-path)
* [`hive-ledger-cli help [COMMAND]`](#hive-ledger-cli-help-command)
* [`hive-ledger-cli limit-order-create PATH OWNER ORDERID AMOUNT_TO_SELL MIN_TO_RECEIVE FILL_OR_KILL EXPIRATION`](#hive-ledger-cli-limit-order-create-path-owner-orderid-amount_to_sell-min_to_receive-fill_or_kill-expiration)
* [`hive-ledger-cli recurrent-transfer PATH FROM TO AMOUNT RECURRENCE EXECUTIONS [MEMO]`](#hive-ledger-cli-recurrent-transfer-path-from-to-amount-recurrence-executions-memo)
* [`hive-ledger-cli transfer PATH FROM TO AMOUNT [MEMO]`](#hive-ledger-cli-transfer-path-from-to-amount-memo)
* [`hive-ledger-cli transfer-to-vesting PATH FROM TO AMOUNT`](#hive-ledger-cli-transfer-to-vesting-path-from-to-amount)
* [`hive-ledger-cli vote PATH VOTER AUTHOR PERMLINK WEIGHT`](#hive-ledger-cli-vote-path-voter-author-permlink-weight)
* [`hive-ledger-cli withdraw-vesting PATH ACCOUNT VESTING_SHARES`](#hive-ledger-cli-withdraw-vesting-path-account-vesting_shares)

## `hive-ledger-cli associate-account USERNAME`

associate account with connected Ledger device. It will replace existing authorities with keys from your device

```
USAGE
  $ hive-ledger-cli associate-account USERNAME

ARGUMENTS
  USERNAME  Account to associate

OPTIONS
  -d, --dry              dry run will only print signed transaction instead broadcasting it
  -h, --help             show CLI help

  -k, --key-gap=key-gap  [default: 5] Gap limit for unused key indexes on which the software decides that SLIP-0048
                         account index is not used

  -t, --testnet          use testnet configuration

EXAMPLE
  $ hive-ledger-cli associate-account test.ledger
  Found existing account:  test.ledger

  This operation will replace ALL your keys with those from your device. First, we need to find new keys for your owner, 
  active and posting authorities.

  Press any key to continue or q to exit:
  Establishing transport with Hive application... done
  Searching for unused keys on your device... done

  New owner key:  STM7ZEBoDotbYpnyNHdARYMDBMNnLWpV7fiiGa6pvHbXhfRo9ZrDf
  Derivation path:  m/48'/13'/0'/1'/0'
  Please confirm this public key on your device... done

  New active key:  STM5zpFRqa73yFULSwUYfPSftx4fE7kha9YfkAPR9yKvNLKU2QDFu
  Derivation path:  m/48'/13'/1'/1'/0'
  Please confirm this public key on your device... done

  New posting key:  STM6YGUesBwuotbZvcfqoXfjQUrfVXGYwJZ9DCHEiFLQfxUsLK9M1
  Derivation path:  m/48'/13'/4'/1'/0'
  Please confirm this public key on your device... done

  Now you need to enter your CURRENT OWNER PRIVATE KEY in WIF format. It will be used to update your account with a new 
  keys. Please be aware that this operation will replace ALL of your current keys

  Enter your current PRIVATE OWNER KEY for test.ledger:
```

_See code: [src/commands/associate-account.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/associate-account.ts)_

## `hive-ledger-cli discover-accounts [ROLE]`

discover accounts associated with connected Ledger device

```
USAGE
  $ hive-ledger-cli discover-accounts [ROLE]

ARGUMENTS
  ROLE  (owner|active|memo|posting) [default: owner] Role to check for

OPTIONS
  -a, --account-gap=account-gap  [default: 5] Gap limit for unused account indexes after which the software decides that
                                 device is not used

  -d, --dry                      dry run will only print signed transaction instead broadcasting it

  -h, --help                     show CLI help

  -k, --key-gap=key-gap          [default: 5] Gap limit for unused key indexes on which the software decides that device
                                 is not used

  -t, --testnet                  use testnet configuration

EXAMPLE
  $ hive-ledger-cli discover-accounts
```

_See code: [src/commands/discover-accounts.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/discover-accounts.ts)_

## `hive-ledger-cli get-public-key PATH`

get public key derived from BIP32 (SLIP-0048) path

```
USAGE
  $ hive-ledger-cli get-public-key PATH

ARGUMENTS
  PATH  BIP 32 path to derive key from

OPTIONS
  -c, --confirm  Force confirmation on Ledger device
  -h, --help     show CLI help

EXAMPLE
  $ hive-ledger-cli get-public-key "m/48'/13'/0'/0'/0'"
  Establishing transport with Hive application... done
  STM5m57x4BXEePAzVNrjUqYeh9C2a7eez1Ya2wPo7ngWLQUdEXjKn
```

_See code: [src/commands/get-public-key.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/get-public-key.ts)_

## `hive-ledger-cli help [COMMAND]`

display help for hive-ledger-cli

```
USAGE
  $ hive-ledger-cli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.3/src/commands/help.ts)_

## `hive-ledger-cli limit-order-create PATH OWNER ORDERID AMOUNT_TO_SELL MIN_TO_RECEIVE FILL_OR_KILL EXPIRATION`

create limit order on internal market to sell or buy HIVE or HBD

```
USAGE
  $ hive-ledger-cli limit-order-create PATH OWNER ORDERID AMOUNT_TO_SELL MIN_TO_RECEIVE FILL_OR_KILL EXPIRATION

ARGUMENTS
  PATH            BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction
  OWNER           account to perform limir order creation
  ORDERID         target account
  AMOUNT_TO_SELL  amount to sell
  MIN_TO_RECEIVE  minimal amount to receive
  FILL_OR_KILL    set to true if you want your limit order to be immediately cancelled if cannot be fulfilled
  EXPIRATION      order expiration date

OPTIONS
  -d, --dry      dry run will only print signed transaction instead broadcasting it
  -h, --help     show CLI help
  -t, --testnet  use testnet configuration

EXAMPLE
  $ hive-ledger-cli limit-order-create "m/48'/13'/0'/0'/0'" engrave 1 "0.001 HIVE" "0.001 HBD" false 
  "2035-10-29T06:32:22"
  Establishing transport with Hive application... done
  Review and confirm transaction on your device... done
  Transaction 1f0c8376cf989483dbb9c3d5fa1006ded00cb6c5 broadcasted successfully

  https://test.ausbit.dev/tx/1f0c8376cf989483dbb9c3d5fa1006ded00cb6c5
```

_See code: [src/commands/limit-order-create.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/limit-order-create.ts)_

## `hive-ledger-cli recurrent-transfer PATH FROM TO AMOUNT RECURRENCE EXECUTIONS [MEMO]`

create recurrent transfer from one account to another

```
USAGE
  $ hive-ledger-cli recurrent-transfer PATH FROM TO AMOUNT RECURRENCE EXECUTIONS [MEMO]

ARGUMENTS
  PATH        BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction
  FROM        sender
  TO          receiver
  AMOUNT      amount to send
  RECURRENCE  hours between consecutive transfers
  EXECUTIONS  how many times should this transfer occur
  MEMO        optional transfer memo

OPTIONS
  -d, --dry      dry run will only print signed transaction instead broadcasting it
  -h, --help     show CLI help
  -t, --testnet  use testnet configuration

EXAMPLE
  $ hive-ledger-cli recurrent-transfer "m/48'/13'/0'/0'/0'" engrave nicniezgrublem "0.001 HIVE" 24 5 "Optional transfer 
  description"
  Establishing transport with Hive application... done
  Review and confirm transaction on your device... done
  Transaction broadcasted successfully with id eb88f7c23cf6e1d183e7bfbd12e204906b33af69

  https://hiveblockexplorer.com/tx/eb88f7c23cf6e1d183e7bfbd12e204906b33af69
```

_See code: [src/commands/recurrent-transfer.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/recurrent-transfer.ts)_

## `hive-ledger-cli transfer PATH FROM TO AMOUNT [MEMO]`

transfer assets from one account to another

```
USAGE
  $ hive-ledger-cli transfer PATH FROM TO AMOUNT [MEMO]

ARGUMENTS
  PATH    BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction
  FROM    sender
  TO      receiver
  AMOUNT  amount to send
  MEMO    optional transfer memo

OPTIONS
  -d, --dry      dry run will only print signed transaction instead broadcasting it
  -h, --help     show CLI help
  -t, --testnet  use testnet configuration

EXAMPLE
  $ hive-ledger-cli transfer "m/48'/13'/0'/0'/0'" engrave nicniezgrublem "0.001 HIVE" "Optional transfer description"
  Establishing transport with Hive application... done
  Review and confirm transaction on your device... done
  Transaction broadcasted successfully with id eb88f7c23cf6e1d183e7bfbd12e204906b33af69

  https://hiveblockexplorer.com/tx/eb88f7c23cf6e1d183e7bfbd12e204906b33af69
```

_See code: [src/commands/transfer.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/transfer.ts)_

## `hive-ledger-cli transfer-to-vesting PATH FROM TO AMOUNT`

convert HIVE to Hive Power, aka stake

```
USAGE
  $ hive-ledger-cli transfer-to-vesting PATH FROM TO AMOUNT

ARGUMENTS
  PATH    BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction
  FROM    source account
  TO      target account
  AMOUNT  amount to stake

OPTIONS
  -d, --dry      dry run will only print signed transaction instead broadcasting it
  -h, --help     show CLI help
  -t, --testnet  use testnet configuration

EXAMPLE
  $ hive-ledger-cli transfer-to-vesting "m/48'/13'/0'/0'/0'" engrave nicniezgrublem "0.001 HIVE"
  Establishing transport with Hive application... done
  Review and confirm transaction on your device... done
  Transaction broadcasted successfully with id eb88f7c23cf6e1d183e7bfbd12e204906b33af69

  https://hiveblockexplorer.com/tx/eb88f7c23cf6e1d183e7bfbd12e204906b33af69
```

_See code: [src/commands/transfer-to-vesting.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/transfer-to-vesting.ts)_

## `hive-ledger-cli vote PATH VOTER AUTHOR PERMLINK WEIGHT`

vote on post or comment

```
USAGE
  $ hive-ledger-cli vote PATH VOTER AUTHOR PERMLINK WEIGHT

ARGUMENTS
  PATH      BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction
  VOTER     voter
  AUTHOR    post author
  PERMLINK  post permlink
  WEIGHT    vote weight (10000 is 100%)

OPTIONS
  -d, --dry      dry run will only print signed transaction instead broadcasting it
  -h, --help     show CLI help
  -t, --testnet  use testnet configuration

EXAMPLE
  $ hive-ledger-cli vote "m/48'/13'/0'/0'/0'" "engrave" "engrave" "introduction"
  Establishing transport with Hive application... done
  Review and confirm transaction on your device... done
  Transaction broadcasted successfully with id a4f9d4694f9e3d45b939d75d3063239f1963f31a

  https://hiveblockexplorer.com/tx/a4f9d4694f9e3d45b939d75d3063239f1963f31a
```

_See code: [src/commands/vote.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/vote.ts)_

## `hive-ledger-cli withdraw-vesting PATH ACCOUNT VESTING_SHARES`

power down your Hive Power and convert it to liquid HIVE

```
USAGE
  $ hive-ledger-cli withdraw-vesting PATH ACCOUNT VESTING_SHARES

ARGUMENTS
  PATH            BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction
  ACCOUNT         account name to power down
  VESTING_SHARES  amount of VESTS to withdraw and convert to liquid HIVE

OPTIONS
  -d, --dry      dry run will only print signed transaction instead broadcasting it
  -h, --help     show CLI help
  -t, --testnet  use testnet configuration

EXAMPLE
  $ hive-ledger-cli withdraw-vesting "m/48'/13'/0'/0'/0'" engrave "20.000000 VESTS"
  Establishing transport with Hive application... done
  Review and confirm transaction on your device... done
  Transaction broadcasted successfully with id eb88f7c23cf6e1d183e7bfbd12e204906b33af69

  https://hiveblockexplorer.com/tx/eb88f7c23cf6e1d183e7bfbd12e204906b33af69
```

_See code: [src/commands/withdraw-vesting.ts](https://gitlab.com/engrave/ledger/hive-ledger-cli/blob/v0.0.0/src/commands/withdraw-vesting.ts)_
<!-- commandsstop -->
